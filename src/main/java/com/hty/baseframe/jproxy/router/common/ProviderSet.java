package com.hty.baseframe.jproxy.router.common;

import com.hty.baseframe.common.util.StringUtil;
import com.hty.baseframe.jproxy.registry.loader.ServiceConsumer;
import com.hty.baseframe.jproxy.registry.loader.ServiceProvider;
import com.hty.baseframe.jproxy.util.ConditionMatchUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ProviderSet {

    private static final Logger logger = LoggerFactory.getLogger(ProviderSet.class);

    private final List<ServiceProvider> providers = Collections.synchronizedList(new ArrayList<ServiceProvider>());

    private int cursor = 0;

    public void add(ServiceProvider provider) {
        synchronized (providers) {
            boolean exists = false;
            for (int i = 0; i < providers.size(); i++) {
                //存在，覆盖
                if (providers.get(i).getUUID().equals(provider.getUUID())
                        && ConditionMatchUtil.isMatch(providers.get(i).getConditions(),
                        provider.getConditions())) {
                    providers.set(i, provider);
                    exists = true;
                    break;
                }
            }
            if (!exists)
                providers.add(provider);
        }
    }

    public ServiceProvider get(ServiceConsumer consumer) {
        synchronized (providers) {
            if (cursor >= providers.size()) {
                cursor = 0;
            }
            for (int i = cursor; i < providers.size(); i++) {
                ServiceProvider provider = providers.get(i);
                int cmp = ConditionMatchUtil.mapCompare(provider.getConditions(),
                        consumer.getConditions());
                if (cmp == 1 || cmp == 0) {
                    cursor++;
                    return provider;
                }
            }
            return null;
        }
    }

    /**
     * 根据客户的筛选条件返回匹配情况
     */
    private boolean matchConditions(ServiceProvider provider, ServiceConsumer consumer) {
        //条件由生产者提供，作为生产者标识自身的标识，但是消费者可以自由提供条件来筛选生产者

        Map<String, String> ccds = consumer.getConditions();
        if (null == ccds || ccds.isEmpty()) {
            return true;
        }
        Map<String, String> pcds = provider.getConditions();
        if (null == pcds || pcds.isEmpty()) {
            return false;
        }
        boolean matchAll = true;
        for (Iterator<Map.Entry<String, String>> it = ccds.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, String> entry = it.next();
            String pvalue = pcds.get(entry.getKey());
            if (!StringUtil.equals(pvalue, entry.getValue())) {
                matchAll = false;
                break;
            }
        }
        return matchAll;
    }

    /**
     * 移除一个ServiceProvider
     */
    public void remove(ServiceProvider provider) {
        synchronized (providers) {
            for (Iterator<ServiceProvider> it = providers.iterator(); it.hasNext(); ) {
                ServiceProvider next = it.next();
                if (next == provider) {
                    it.remove();
                    break;
                }
            }
        }
    }

    /**
     * 根据provider的UUID移除
     */
    public void remove(String uuid) {
        synchronized (providers) {
            for (Iterator<ServiceProvider> it = providers.iterator(); it.hasNext(); ) {
                ServiceProvider next = it.next();
                if (next.getUUID().equals(uuid)) {
                    logger.info("Remove provider:" + next);
                    it.remove();
                    break;
                }
            }
        }
    }
}
