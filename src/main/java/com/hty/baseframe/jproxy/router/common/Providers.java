package com.hty.baseframe.jproxy.router.common;

import com.hty.baseframe.jproxy.registry.loader.ServiceProvider;

import java.util.*;

public class Providers {

    private static Map<String, ProviderSet> providers =
            Collections.synchronizedMap(new HashMap<String, ProviderSet>());

    private Providers() {
    }

    public static ProviderSet provider(String clazz) {
        synchronized (providers) {
            ProviderSet versions = providers.get(clazz);
            if (null == versions) {
                versions = new ProviderSet();
                providers.put(clazz, versions);
            }
            return versions;
        }
    }

    public static void add(ServiceProvider provider) {
        provider(provider.getClazz()).add(provider);
    }

    public static void remove(String clazz) {
        synchronized (providers) {
            providers.remove(clazz);
        }
    }

    public static void removeByUUID(String uuid) {
        synchronized (providers) {
            for (Iterator<ProviderSet> it = providers.values().iterator(); it.hasNext(); ) {
                ProviderSet set = it.next();
                set.remove(uuid);
            }
        }
    }

}
