package com.hty.baseframe.jproxy.router.listener;

import com.hty.baseframe.jproxy.router.common.ProviderZoom;
import com.hty.baseframe.jproxy.server.SocketListener;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSocketListener implements SocketListener {

	private static final Logger logger = LoggerFactory.getLogger(SimpleSocketListener.class);

	public void connect(IoSession session) {
		logger.debug("/new connection comes in.");
		ProviderZoom.initProviderState(session);
	}

	public void disconnect(IoSession session) {
		logger.debug("/connection disconnected.");
		ProviderZoom.clearProviderState(session);
	}

}
