package com.hty.baseframe.jproxy.router.service;

import com.hty.baseframe.jproxy.registry.ServiceRegistryService;
import com.hty.baseframe.jproxy.registry.loader.CandidateProvider;
import com.hty.baseframe.jproxy.registry.loader.ServiceConsumer;
import com.hty.baseframe.jproxy.registry.loader.ServiceProvider;
import com.hty.baseframe.jproxy.router.common.ProviderZoom;
import com.hty.baseframe.jproxy.tunel.server.ServerTunnelHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class ServiceRegistryServerImpl implements ServiceRegistryService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public boolean expose(ServiceProvider provider) {
        ProviderZoom.addProvider(provider);
        return true;
    }

    public CandidateProvider find(ServiceConsumer consumer) {
        logger.info("Find provider for consumer: {}", consumer);
        IoSession session = ServerTunnelHandler.getCurrentIoSession();
        String lookBackAddress = ((InetSocketAddress) session.getRemoteAddress()).getAddress().getHostAddress();
        ServiceProvider prover = ProviderZoom.getProvider(consumer);
        return null == prover ? null : prover.clone(lookBackAddress);
    }
}
