package com.hty.baseframe.jproxy.router;

import com.hty.baseframe.jproxy.JProxy;
import com.hty.baseframe.jproxy.bean.LocalService;
import com.hty.baseframe.jproxy.common.BeanProvider;
import com.hty.baseframe.jproxy.common.ServiceFactory;
import com.hty.baseframe.jproxy.common.SysProperties;
import com.hty.baseframe.jproxy.registry.ServiceRegistryService;
import com.hty.baseframe.jproxy.router.common.SimpleBeanProvider;
import com.hty.baseframe.jproxy.router.listener.SimpleSocketListener;
import com.hty.baseframe.jproxy.server.SocketListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class JProxyServer extends JProxy {

    private void initProperties() throws IOException {
        URL url = this.getClass().getClassLoader().getResource(JProxyServer.class.getPackage().getName().replace(".", "/"));
        String mainClassPath = url.getPath();
        if (mainClassPath.startsWith("file:")) {
            mainClassPath = mainClassPath.substring(5);
        }
        if (mainClassPath.contains("!")) {
            mainClassPath = mainClassPath.substring(0, mainClassPath.indexOf("!"));
        }
        File classpath = null;
        if (mainClassPath.endsWith(".jar")) {
            classpath = new File(mainClassPath).getParentFile();
        } else {
            classpath = new File("").getAbsoluteFile();
        }
        File propFile = new File(classpath, "router.properties");
        InputStream ips = new FileInputStream(propFile);
        Properties props = new Properties();
        props.load(ips);
        String _port = props.getProperty("port");
        String _max_connection = props.getProperty("max_connection");
        SysProperties.addProperty("local_service_port", _port);
        SysProperties.addProperty("max_connection", _max_connection);
        BeanProvider provider = new SimpleBeanProvider();
        ServiceFactory.setBeanProvider(provider);
    }

    /**
     * 初始化特殊的LocalService：ServiceRegistryServer
     */
    private void initLocalService() {
        LocalService locaService = new LocalService(ServiceRegistryService.class, null);
        ServiceFactory.addLocalService(locaService);
    }

    /**
     * 内存中配置的初始化
     */
    private void initInMemoryConfig() throws IOException {
        initProperties();
        initLocalService();
    }

    public void startServer() throws Exception {
        SocketListener listener = new SimpleSocketListener();
        this.printWelcome();
        initInMemoryConfig();
        this.start1(listener);
    }
}
