package com.hty.baseframe.jproxy.router.common;

import com.hty.baseframe.jproxy.registry.loader.ServiceConsumer;
import com.hty.baseframe.jproxy.registry.loader.ServiceProvider;
import com.hty.baseframe.jproxy.tunel.server.ServerTunnelHandler;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.*;

/**
 * 保存服务提供者
 *
 * @author Hetianyi 2017/12/30
 * @version 1.0
 */
public class ProviderZoom {

    private static final Logger logger = LoggerFactory.getLogger(ProviderZoom.class);

    // 按连接分类  <session id -->  provider's UUID>
    private static Map<Integer, Set<String>> providerState =
            Collections.synchronizedMap(new HashMap<Integer, Set<String>>());

    /**
     * 该方法由socketlistener调用
     */
    public static synchronized void initProviderState(IoSession session) {
        int socketHash = session.hashCode();
        Set<String> ps = providerState.get(socketHash);
        synchronized (providerState) {
            if (null == ps) {
                ps = Collections.synchronizedSet(new HashSet<String>());
                providerState.put(socketHash, ps);
            }
        }
    }

    /**
     * Mina会话断开，则经此session注册的提供者都要注销
     */
    public static synchronized void clearProviderState(IoSession session) {
        int hasCode = session.hashCode();
        synchronized (providerState) {
            Set<String> ps = providerState.get(hasCode);
            if (null != ps) {
                for (Iterator<String> it = ps.iterator(); it.hasNext(); ) {
                    String uuid = it.next();
                    Providers.removeByUUID(uuid);
                }
            }
            providerState.remove(hasCode);
        }
    }

    /**
     * 添加一个服务提供者
     */
    public static synchronized boolean addProvider(ServiceProvider provider) {
        if (null == provider) {
            logger.error("Provider is null, so will not add!");
            return false;
        }
        IoSession session = ServerTunnelHandler.getCurrentIoSession();
        String lookBackAddress = ((InetSocketAddress) session.getRemoteAddress()).getAddress().getHostAddress();
        provider.setProviderLookBackAddress(lookBackAddress);
        provider.setSessionId(session.getId());
        logger.info("Add provider: {}", provider);
        Providers.add(provider);
        synchronized (providerState) {
            int hasCode = session.hashCode();
            Set<String> ps = providerState.get(hasCode);
            if (null == ps) {
                ps = new HashSet<String>();
                providerState.put(hasCode, ps);
            }
            ps.add(provider.getUUID());
        }
        return true;
    }

    /**
     * 获取Provider
     *
     * @param consumer
     * @return
     */
    public static ServiceProvider getProvider(ServiceConsumer consumer) {
        ServiceProvider provider = Providers.provider(consumer.getClazz()).get(consumer);
        return provider;
    }
}
