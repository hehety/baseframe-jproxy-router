package com.hty.baseframe.jproxy.router;

/**
 * 启动类
 */
public class Main {

    public static void main(String[] args) throws Exception {
        JProxyServer jp = new JProxyServer();
        jp.startServer();
    }
}
