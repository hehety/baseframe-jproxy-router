package com.hty.baseframe.jproxy.router.common;

import com.hty.baseframe.jproxy.common.BeanProvider;
import com.hty.baseframe.jproxy.registry.ServiceRegistryService;
import com.hty.baseframe.jproxy.router.service.ServiceRegistryServerImpl;

import java.util.HashMap;
import java.util.Map;

public class SimpleBeanProvider implements BeanProvider {

    private Map<Class<?>, Object> instances = new HashMap<Class<?>, Object>();

    public Object getBean(Class<?> type) {
        if (type == ServiceRegistryService.class) {
            Object obj = instances.get(type);
            if (null == obj) {
                obj = new ServiceRegistryServerImpl();
            }
            return obj;
        }
        return null;
    }
}
